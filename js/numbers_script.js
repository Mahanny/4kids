function choose(obj,audio1,audio2)
{
    var wait;
    if(obj=="btn1")
    {
        document.getElementById("btn1").style.backgroundColor="green";
        document.getElementById("btn2").style.backgroundColor="";
        document.getElementById("btn3").style.backgroundColor="";
        document.getElementById("btn2").style.visibility = "hidden";
        document.getElementById("btn3").style.visibility = "hidden";

        document.getElementById(audio1).play();
        document.getElementById(audio2).pause();
        

        setTimeout(function () {
            document.getElementById("div1").style.display="none";
            document.getElementById("div3").style.display="inline";
            document.getElementById("div5").style.display="none";
            document.getElementById("div4").style.display="none";
            document.getElementById("div2").style.display="none";
          }, 3000);
    }
    else if(obj == "btn2" )
    { 
        document.getElementById("btn2").style.backgroundColor="red";
        document.getElementById("btn1").style.backgroundColor="";
        document.getElementById("btn3").style.backgroundColor="";

        document.getElementById(audio1).play();
        document.getElementById(audio2).pause();
    }
    else
    {
        document.getElementById("btn3").style.backgroundColor="red";
        document.getElementById("btn2").style.backgroundColor="";
        document.getElementById("btn1").style.backgroundColor="";
        
        document.getElementById(audio1).play();
        document.getElementById(audio2).pause();
    }
  
}

function choose_2(obj,audio1,audio2)
{
    var wait;
    if(obj=="btn1")
    {
        
       // document.getElementsByName("3").style.backgroundColor="green";alert(obj);
        document.getElementById("btn2_1").style.backgroundColor="green";
        document.getElementById("btn2_2").style.backgroundColor="";
        document.getElementById("btn2_3").style.backgroundColor="";

        document.getElementById("btn2_2").style.visibility = "hidden";
        document.getElementById("btn2_3").style.visibility = "hidden";

        document.getElementById(audio1).play();
        document.getElementById(audio2).pause();

        setTimeout(function () {
            document.getElementById("div5").style.display="inline";
            document.getElementById("div1").style.display="none";
            document.getElementById("div3").style.display="none";
            document.getElementById("div4").style.display="none";
            document.getElementById("div2").style.display="none";
          }, 3000);
    }
    else if(obj == "btn2" )
    { 
        
        document.getElementById("btn2_2").style.backgroundColor="red";
        document.getElementById("btn2_1").style.backgroundColor="";
        document.getElementById("btn2_3").style.backgroundColor="";
        document.getElementById(audio1).play();
        document.getElementById(audio2).pause();
    }
    else
    {
        document.getElementById("btn2_3").style.backgroundColor="red";
        document.getElementById("btn2_2").style.backgroundColor="";
        document.getElementById("btn2_1").style.backgroundColor="";
        document.getElementById(audio1).play();
        document.getElementById(audio2).pause();
    }
  
}


function choose_3(obj,audio1,audio2)
{
    var wait;
    if(obj=="btn1")
    {
        document.getElementById("btn3_1").style.backgroundColor="green";  
        document.getElementById("btn3_2").style.backgroundColor="";
        document.getElementById("btn3_3").style.backgroundColor="";

        document.getElementById("btn3_2").style.visibility = "hidden";
        document.getElementById("btn3_3").style.visibility = "hidden";

        document.getElementById(audio1).play();
        document.getElementById(audio2).pause();

        setTimeout(function () {
            document.getElementById("div5").style.display="none";
            document.getElementById("div1").style.display="none";
            document.getElementById("div3").style.display="none";
            document.getElementById("div4").style.display="inline";
            document.getElementById("div2").style.display="none";
          }, 3000);
    }
    else if(obj == "btn2" )
    { 
        document.getElementById("btn3_2").style.backgroundColor="red";
        document.getElementById("btn3_1").style.backgroundColor="";
        document.getElementById("btn3_3").style.backgroundColor="";
        document.getElementById(audio1).play();
        document.getElementById(audio2).pause();
    }
    else
    {
        document.getElementById("btn3_3").style.backgroundColor="red";
        document.getElementById("btn3_2").style.backgroundColor="";
        document.getElementById("btn3_1").style.backgroundColor="";
        document.getElementById(audio1).play();
        document.getElementById(audio2).pause();
    }
  
}
function choose_4(obj,audio1,audio2)
{
    var wait;
    if(obj=="btn1")
    {
        document.getElementById("btn4_1").style.backgroundColor="green";        
        document.getElementById("btn4_2").style.backgroundColor="";
        document.getElementById("btn4_3").style.backgroundColor="";

        document.getElementById("btn4_2").style.visibility = "hidden";
        document.getElementById("btn4_3").style.visibility = "hidden";


        document.getElementById(audio1).play();
        document.getElementById(audio2).pause();

        setTimeout(function () {
            document.getElementById("div2").style.display="inline";
            document.getElementById("div4").style.display="none";
            document.getElementById("div5").style.display="none";
            document.getElementById("div1").style.display="none";
            document.getElementById("div3").style.display="none";
            
          
          }, 3000);
    }
    else if(obj == "btn2" )
    { 
        document.getElementById("btn4_2").style.backgroundColor="red";
        document.getElementById("btn4_1").style.backgroundColor="";
        document.getElementById("btn4_3").style.backgroundColor="";
        document.getElementById(audio1).play();
        document.getElementById(audio2).pause();
    }
    else
    {
        document.getElementById("btn4_3").style.backgroundColor="red";
        document.getElementById("btn4_2").style.backgroundColor="";
        document.getElementById("btn4_1").style.backgroundColor="";
        document.getElementById(audio1).play();
        document.getElementById(audio2).pause();
    }
  
}

function choose_5(obj,audio1,audio2)
{
    var wait;
    if(obj=="btn1")
    {
        document.getElementById("btn4_1").style.backgroundColor="green";        
        document.getElementById("btn4_2").style.backgroundColor="";
        document.getElementById("btn4_3").style.backgroundColor="";
        
        document.getElementById("btn5_2").style.visibility = "hidden";
        document.getElementById("btn5_3").style.visibility = "hidden";

        document.getElementById(audio1).play();
        document.getElementById(audio2).pause();

        setTimeout(function () {
            document.getElementById("div2").style.display="inline";
            document.getElementById("div4").style.display="none";
            document.getElementById("div5").style.display="none";
            document.getElementById("div1").style.display="none";
            document.getElementById("div3").style.display="none";
            
          
          }, 3000);
    }
    else if(obj == "btn2" )
    { 
        document.getElementById("btn5_2").style.backgroundColor="red";
        document.getElementById("btn5_1").style.backgroundColor="";
        document.getElementById("btn5_3").style.backgroundColor="";
        document.getElementById(audio1).play();
        document.getElementById(audio2).pause();
    }
    else
    {
        document.getElementById("btn5_3").style.backgroundColor="red";
        document.getElementById("btn5_2").style.backgroundColor="";
        document.getElementById("btn5_1").style.backgroundColor="";
        document.getElementById(audio1).play();
        document.getElementById(audio2).pause();
    }
  
}

